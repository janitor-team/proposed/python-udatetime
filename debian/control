Source: python-udatetime
Section: python
Priority: optional
Maintainer: Ilias Tsitsimpis <iliastsi@debian.org>
Rules-Requires-Root: no
Uploaders: Debian Python Team <team+python@tracker.debian.org>
Build-Depends:
 debhelper-compat (= 12),
 dh-python,
 python3-all,
 python3-all-dev,
 python3-setuptools
Standards-Version: 4.5.0
Homepage: https://pypi.python.org/pypi/udatetime
Vcs-Git: https://salsa.debian.org/python-team/packages/python-udatetime.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-udatetime
Testsuite: autopkgtest-pkg-python

Package: python3-udatetime
Architecture: any
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends}
Description: fast RFC3339 compliant date-time library (Python 3)
 This module offers faster datetime object instantiation, serialization and
 deserialization of RFC3339 date-time strings, than Python's datetime module.
 It uses Python's datetime class under the hood so, code already using datetime
 should be able to easily switch to udatetime. All datetime objects created
 by udatetime are timezone aware.
 .
 This package installs the library for Python 3.
